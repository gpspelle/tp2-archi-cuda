/*********************************************************************************/
/* Matrix product program for a multi-core CPU and for a many-core GPU           */
/* S. Vialle - October 2020                                                      */
/*********************************************************************************/

#ifndef __MATPROD_INIT__
#define __MATPROD_INIT__

void LocalMatrixInit(void);                      // Data init

void usage(int ExitCode, FILE *std);             // Cmd line parsing and usage
void CommandLineParsing(int argc, char *argv[], int *, int*);

void PrintResultsAndPerf(double gigaflops, double d1, int ongpu); // Res printing

void CheckResults(void); // Res checking

#endif

// END
