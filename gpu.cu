/*********************************************************************************/
/* Matrix product program for a multi-core CPU and for a many-core GPU           */
/* S. Vialle - October 2020                                                      */
/*********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <cuda.h> 
#include <cuda_runtime.h>
#include <cublas_v2.h>

#include "main.h"
#include "gpu.h"


/*-------------------------------------------------------------------------------*/
/* GPU symbols                                                                   */
/*-------------------------------------------------------------------------------*/
__device__ T_real GPU_A[SIZE][SIZE];
__device__ T_real GPU_B[SIZE][SIZE];
__device__ T_real GPU_C[SIZE][SIZE];

// New Symbol and vars to call cublas
__device__ T_real GPU_Ctmp[SIZE][SIZE];

T_real *AdrGPU_A = NULL;
T_real *AdrGPU_B = NULL;
T_real *AdrGPU_C = NULL;
T_real *AdrGPU_Ctmp = NULL;

cublasHandle_t cublasHandle;

/*-------------------------------------------------------------------------------*/
/* Init and finalize the GPU device.                                             */
/*-------------------------------------------------------------------------------*/
void gpuInit(void)
{
    cuInit(0);

    // Extract address of GPU matrix "symbols"
    // TO DO TO DO TO DO
    
    // Turn CPU arrays A, B and C into "pinned" memory areas
    // TO DO TO DO TO DO
    
    // Initialize CUBLAS lib usage
    CHECK_CUBLAS_SUCCESS(cublasCreate(&cublasHandle), "Init of the CUBLAS lib handle"); 
}


void gpuFinalize(void)
{
    // Turn "pinned" CPU arrays into std array
    // TO DO TO DO TO DO
    
    // Free CUBLAS lib usage
    CHECK_CUBLAS_SUCCESS(cublasDestroy(cublasHandle), "Free the CUBLAS lib");
}


/*-------------------------------------------------------------------------------*/
/* Transfer of CPU input data into GPU symbols                                   */
/*-------------------------------------------------------------------------------*/
void gpuSetDataOnGPU(void)
{
 // Set GPU_A symbol
 CHECK_CUDA_SUCCESS(cudaMemcpyToSymbol(GPU_A, &A[0][0], sizeof(T_real)*SIZE*SIZE, 0, cudaMemcpyHostToDevice),"Transfer A-->GPU_A");

 // Set GPU_B symbol
 CHECK_CUDA_SUCCESS(cudaMemcpyToSymbol(GPU_B, &B[0][0], sizeof(T_real)*SIZE*SIZE, 0, cudaMemcpyHostToDevice),"Transfer B-->GPU_B");
}


/*-------------------------------------------------------------------------------*/
/* Transfer of GPU results into CPU array                                        */
/*-------------------------------------------------------------------------------*/
void gpuGetResultOnCPU(void)
{
 // Get GPU_C symbol
 CHECK_CUDA_SUCCESS(cudaMemcpyFromSymbol(&C[0][0], GPU_C, sizeof(T_real)*SIZE*SIZE, 0, cudaMemcpyDeviceToHost),"Transfer GPU_C-->C");
}


/*-------------------------------------------------------------------------------*/
/* Small matrix product on the local GPU.                                    */
/*-------------------------------------------------------------------------------*/
__global__ void MatrixProductKernel_v0(void)
{
 T_real res = 0.0; 

 int lig = threadIdx.y + blockIdx.y*BLOCK_SIZE_Y_K0;
 int col = threadIdx.x + blockIdx.x*BLOCK_SIZE_X_K0;

 if (lig < SIZE && col < SIZE) {
   for(int k = 0; k < SIZE; k++) {
     res += GPU_A[lig][k] * GPU_B[k][col];
   }

   GPU_C[lig][col] = res;
 }
}


/*-------------------------------------------------------------------------------*/
/* Small matrix product on the local GPU - 2D & generic matrix size              */
/*-------------------------------------------------------------------------------*/
__global__ void MatrixProductKernel_v1(int BLOCK_SIZE_X_K1, int BLOCK_SIZE_Y_K1)
{

 T_real res = 0.0; 

 int lig = threadIdx.y + blockIdx.y*BLOCK_SIZE_Y_K1;
 int col = threadIdx.x + blockIdx.x*BLOCK_SIZE_X_K1;

 if (lig < SIZE && col < SIZE) {
   for(int k = 0; k < SIZE; k++) {
     res += GPU_A[lig][k] * GPU_B[k][col];
   }

   GPU_C[lig][col] = res;
 }
}


/*-------------------------------------------------------------------------------*/
/* Transposition kernel using global memory and registers.                       */
/*-------------------------------------------------------------------------------*/
__global__ void TransposeKernel_v0(T_real *MT, T_real *M, int mLig, int nCol)
{
 int lig = threadIdx.y + blockIdx.y*BLOCK_SIZE_XY_KT0;
 int col = threadIdx.x + blockIdx.x*BLOCK_SIZE_XY_KT0;
 
 if (lig < mLig && col < nCol)
   MT[col*mLig + lig] = M[lig*nCol + col];
}


/*-------------------------------------------------------------------------------*/
/* Transposition kernel using global shared memory.                              */
/*-------------------------------------------------------------------------------*/
__global__ void TransposeKernel_v1(T_real *MT, T_real *M, int mLig, int nCol)
{
 // Natural lig-col coordinates of the thread (leading to coalescent reading)
 int firstLibBlock = blockIdx.y*BLOCK_SIZE_XY_KT1;
 int firstColBlock = blockIdx.x*BLOCK_SIZE_XY_KT1;
 int lig = firstLibBlock + threadIdx.y;
 int col = firstColBlock + threadIdx.x;
 
 // Not natural ligT-colT coordinates of the threadin the transpose matrix
 // in order to achieve coalescent writting
 int ligT = firstColBlock + threadIdx.y;
 int colT = firstLibBlock + threadIdx.x;
 
 // shared memory matrix block
 __shared__ T_real shM[BLOCK_SIZE_XY_KT1][BLOCK_SIZE_XY_KT1];
 
 // Coalescent reading of M matrix, and storage in shm matrix block
 if (lig < mLig && col < nCol)
   shM[threadIdx.y][threadIdx.x] = M[lig*nCol + col];
 // Waiting all data are stored in the shm block matrix
 __syncthreads();
 // Coalescent writing in the transposed matrix
 if (ligT < nCol && colT < mLig)
   MT[(firstColBlock + threadIdx.y)*mLig + 
      (firstLibBlock + threadIdx.x)        ] = shM[threadIdx.x][threadIdx.y];
}


/*-------------------------------------------------------------------------------*/
/* Small matrix product on the local GPU.                                        */
/*-------------------------------------------------------------------------------*/
void gpuProduct(gkid_t kid, int BLOCK_SIZE_X_K1, int BLOCK_SIZE_Y_K1)
{
    dim3 Dg, Db;
    // ligne goes first
    // column goes secon
    // mat[lig][column]
    // ligne goes on -y direction
    // column goes on +x direction 
    // using traditional cartesian plane

    switch(kid) {

        case GK0 : // Kernel v0 - using only global memory (with coalescent data accesses) 
          // - init the grid of blocs
          Db.x = BLOCK_SIZE_X_K0;
          Db.y = 1;
          Db.z = 1;
	  Dg.x = SIZE/BLOCK_SIZE_X_K0+ (SIZE%BLOCK_SIZE_X_K0 ? 1 : 0) ;
          Dg.y = SIZE;
          Dg.z = 1;
          // - run the Grid of Blocs of threads
          MatrixProductKernel_v0<<<Dg,Db>>>();
          break;

        case GK1 :
	  // - init the grid of blocs
          Db.x = BLOCK_SIZE_X_K1;
          Db.y = BLOCK_SIZE_Y_K1;
          Db.z = 1;
	  Dg.x = (SIZE + BLOCK_SIZE_X_K1 - 1) / BLOCK_SIZE_X_K1;
          Dg.y = (SIZE + BLOCK_SIZE_Y_K1 - 1) / BLOCK_SIZE_Y_K1;
          Dg.z = 1;
          // - run the Grid of Blocs of threads
          MatrixProductKernel_v1<<<Dg,Db>>>(BLOCK_SIZE_X_K1, BLOCK_SIZE_Y_K1);

          break;

        case GK2 :
            break;

        case GK3 :
            break;

        case GK4 :
            break;

        case GK5 :
            break;

        case GK6 :
            break;

        case GK7 :
            break;

        case GK8 :
            break;

        default :
            fprintf(stderr,"Unknown GPU kernel!");
            exit(EXIT_FAILURE);
    }
}




