#!/bin/bash
for (( i = 1; i <= 1024; i*=2 ))
do

    for (( j = 1 ; j <= 1024 / i; j*=2 ))
    do
	  echo "************** START RESULTS FOR x-size: $i - y-size: $j *******************"
	  echo "************** START RESULTS FOR x-size: $i - y-size: $j *******************" >> out
          MatrixProduct -t GPU -gpu-k 1 -x-k1-size $i -y-k1-size $j >> out 
	  echo "************** END RESULTS FOR x-size: $i - y-size: $j *******************" >> out
	  echo "************** END RESULTS FOR x-size: $i - y-size: $j *******************"
    done
done
